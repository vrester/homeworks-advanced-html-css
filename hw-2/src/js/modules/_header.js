const dropDownMenu = document.querySelector('.menu__icon')
const menuList = document.querySelector('.menu__list')


dropDownMenu.addEventListener('click', () => {
    if (menuList) {
        menuList.classList.toggle('menu__list-active')
        dropDownMenu.classList.toggle('menu__icon-active')
    }
})

