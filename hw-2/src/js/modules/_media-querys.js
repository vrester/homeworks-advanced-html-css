const newsCards = document.querySelectorAll('.news-card');
const instShots = document.querySelectorAll('.inst__link-photo');

const mediaQueries = [
    {
        query: "(max-width: 420px)",
        lengths: [3, 1],
        targets: [newsCards, instShots],
        classNames: ['news-card--active', 'inst__link-photo--active']
    },
    {
        query: "(min-width: 421px) and (max-width: 860px)",
        lengths: [4, 3],
        targets: [newsCards, instShots],
        classNames: ['news-card--active', 'inst__link-photo--active']
    },
    {
        query: "(min-width: 861px)",
        lengths: [4, 4],
        targets: [newsCards, instShots],
        classNames: ['news-card--active', 'inst__link-photo--active']
    }
];

mediaQueries.forEach(mediaQuery => {
    const mq = window.matchMedia(mediaQuery.query);
    mq.addEventListener('change', isMediaMatches);
    isMediaMatches(mq);
});

function isMediaMatches(mq) {
    if (mq.matches) {
        const mediaQueryIndex = mediaQueries.findIndex(q => q.query === mq.media);
        const {lengths, targets, classNames} = mediaQueries[mediaQueryIndex];
        addActive(lengths, targets, classNames);
    }
}

function addActive(lengths, targets, classNames) {
    targets.forEach((target, index) => {
        target.forEach((element, i) => {
            if (i < lengths[index]) {
                element.classList.add(classNames[index]);
            } else {
                element.classList.remove(classNames[index]);
            }
        });
    });
}
