// Main module
import gulp from 'gulp'

// Path
import {path} from "./gulp/config/path.js"

//Import plugins
import {plugins} from "./gulp/config/plugins.js"

// Import tasks
import {reset} from "./gulp/tasks/reset.js"
import {html} from "./gulp/tasks/html.js"
import {server} from "./gulp/tasks/server.js"
import {scss} from "./gulp/tasks/scss.js"
import {js} from "./gulp/tasks/js.js"
import {images} from "./gulp/tasks/images.js"
import {fontsStyle, otfToTtf, ttfToWoff} from "./gulp/tasks/fonts.js";
import {zip} from "./gulp/tasks/zip.js"

// Global object with variables
global.app = {
    isBuild: process.argv.includes('--build'),
    isDev: !process.argv.includes('--build'),
    path: path,
    gulp: gulp,
    plugins: plugins,
}


// Watcher
function watcher() {
    gulp.watch(path.watch.html, html)
    gulp.watch(path.watch.scss, scss)
    gulp.watch(path.watch.js, js)
    gulp.watch(path.watch.images, images)
}

// Fonts
const fonts = gulp.series(otfToTtf, ttfToWoff, fontsStyle)
// Tasks scenarios
const mainTasks = gulp.series(fonts, gulp.parallel(html, scss, js, images))

const dev = gulp.series(reset, mainTasks, gulp.parallel(watcher, server))
const build = gulp.series(reset, mainTasks)
const deployZIP = gulp.series(reset, mainTasks, zip)

export {dev}
export {build}
export {deployZIP}

//Default scenarios
gulp.task('default', dev)