import plumber from "gulp-plumber" // Error processing
import notify from "gulp-notify" // Desktop notify
import browserSync from "browser-sync" // Local server
import newer from "gulp-newer" // Checking for updates
import ifPlugin from "gulp-if"

export const plugins = {
    plumber: plumber,
    notify: notify,
    browserSync: browserSync,
    newer: newer,
    if: ifPlugin,
}